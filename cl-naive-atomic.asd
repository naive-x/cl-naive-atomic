(defsystem "cl-naive-atomic"
  :description "Implements some naive atomic operations."
  :version "2021.6.29"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ("bordeaux-threads")
  :components ((:file "src/packages")
	           (:file "src/api"          :depends-on ("src/packages"))
               (:file "src/cas"          :depends-on ("src/api"))
               (:file "src/bt-lock"      :depends-on ("src/api"))
	           (:file "src/mvar"         :depends-on ("src/api" "src/cas"))
	           (:file "src/atomic"       :depends-on ("src/api" "src/mvar"))
	           (:file "src/channel"      :depends-on ("src/api" "src/mvar"))))
