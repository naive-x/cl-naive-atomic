(in-package :cl-naive-atomic.tests)

(testsuite 1-slot-channel/single-thread
  (dolist (*default-mvar-constructor* *mvar-constructors*)
    (let ((channel (make-channel)))

      (testcase put/1
                :equal 'eql
                :expected 33
                :actual (complete-before 1 (put channel 33))
                :info (scat '1-slot-channel/ *default-mvar-constructor*
                            " The result of (put channel) is the new value"))

      (testcase take/1
                :equal 'eql
                :expected 33
                :actual (complete-before 1 (take channel))
                :info (scat '1-slot-channel/ *default-mvar-constructor*
                            " The channel take back the put value."))

      (testcase put-take/1
                :equal 'eql
                :expected 22
                :actual (complete-before 1 (progn (put channel 22) (take channel)))
                :info (scat '1-slot-channel/ *default-mvar-constructor*
                            " put take sequence.")))))

(testsuite unbounded-channel/single-thread
  (let* ((n    1000)
         (list (iota n)))
    (dolist (*default-mvar-constructor* *mvar-constructors*)
      (let ((channel (make-unbounded-channel)))

        (testcase put/1
                  :equal 'eql
                  :expected 33
                  :actual (complete-before 1 (put channel 33))
                  :info (scat 'unbounded-channel/ *default-mvar-constructor*
                              " The result of (put channel) is the new value"))

        (testcase take/1
                  :equal 'eql
                  :expected 33
                  :actual (complete-before 1 (take channel))
                  :info (scat 'unbounded-channel/ *default-mvar-constructor*
                              " The channel take back the put value."))

        (testcase put-take/1
                  :equal 'eql
                  :expected 22
                  :actual (complete-before 1 (progn (put channel 22) (take channel)))
                  :info (scat 'unbounded-channel/ *default-mvar-constructor*
                              " put take sequence."))

        (testcase put/n
                  :equal 'eql
                  :expected nil
                  :actual (complete-before 2 (dolist (i list (put channel nil))
                                               (put channel i)))
                  :info (scat 'unbounded-channel/ *default-mvar-constructor*
                              " The result of (put channel i) is the new value"))

        (testcase take/n
                  :equal 'equal
                  :expected list
                  :actual (complete-before 2
                                           (loop :for i := (take channel)
                                                 :until (null i)
                                                 :collect i))
                  :info (scat 'unbounded-channel/ *default-mvar-constructor*
                              " The channel take back the put values."))

        (testcase put-take/n
                  :equal 'eql
                  :expected nil
                  :actual (complete-before
                           2
                           (not (loop
                                  :for i :in list
                                  :do (put channel i)
                                  :always (eql i (take channel)))))
                  :info (scat 'unbounded-channel/ *default-mvar-constructor*
                              " put take sequence."))))))

(testsuite consummer-channel
  (let* ((n    1000)
         (list (iota n)))
    (dolist (*default-mvar-constructor* *mvar-constructors*)
      (dolist (unbounded '(nil t))
        (testcase collect
                  :equal 'equal
                  :expected list
                  :actual (complete-before
                           10
                           (let ((result '()))
                             (flet ((consummer (item) (push item result)))
                               (multiple-value-bind (channel thread)
                                   (make-consummer-channel
                                    (function consummer)
                                    :eof-value nil
                                    :unbounded unbounded)
                                 (loop
                                   :for i :in list
                                   :do (put channel i))
                                 (put channel nil)
                                 (bt:join-thread thread)
                                 (nreverse result)))))
                  :info (scat (if unbounded
                                  'unbounded-channel/
                                  '1-slot-channel/)
                              *default-mvar-constructor*
                              " consumme a list."))))))

(testsuite producer-channel
  (let* ((n    1000)
         (list (iota n)))
    (dolist (*default-mvar-constructor* *mvar-constructors*)
      (dolist (unbounded '(nil t))
        (testcase collect
                  :equal 'equal
                  :expected list
                  :actual (complete-before
                           10
                           (let ((current list))
                             (flet ((producer () (pop current)))
                               (multiple-value-bind (channel thread)
                                   (make-producer-channel
                                    (function producer)
                                    :eof-value nil
                                    :unbounded unbounded)
                                 (prog1 (loop
                                          :for i := (take channel)
                                          :while i
                                          :collect i)
                                   (bt:join-thread thread))))))
                  :info (scat (if unbounded
                                  'unbounded-channel/
                                  '1-slot-channel/)
                              *default-mvar-constructor*
                              " produce a list."))))))

(testsuite channel-pipe
  (let* ((n    1000)
         (list (iota n)))
    (dolist (*default-mvar-constructor* *mvar-constructors*)
      (dolist (unbounded '(nil t))
        (testcase collect
                  :equal 'equal
                  :expected list
                  :actual (complete-before
                           20
                           (let ((current list)
                                 (result '()))
                             (flet ((producer  ()     (pop current))
                                    (consummer (item) (push item result)))
                               (channel-pipe
                                (function producer)
                                (function consummer)
                                :eof-value nil
                                :unbounded unbounded
                                :wait t)
                               (nreverse result))))
                  :info (scat (if unbounded
                                  'unbounded-channel/
                                  '1-slot-channel/)
                              *default-mvar-constructor*
                              " pipe a list from a producer to a consummer."))))))

