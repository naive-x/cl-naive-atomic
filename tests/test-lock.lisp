(in-package :cl-naive-atomic.tests)

'(
   :*default-lock-class*  :make-lock
   :locking-thread
   :call-with-lock-held
   :with-lock-held
   :*default-condition-variable-class* :make-condition-variable
   :condition-notify
   :condition-wait)

(defun counter-increment-by-threads (nthreads niterations)
  (let* ((lock (make-lock))
         (counter 0)
         (threads
           (loop
             :repeat nthreads
             :collect (bt:make-thread
                       (lambda ()
                         (loop
                           :repeat niterations
                           :do (with-lock-held (lock) (incf counter))))))))
    (mapc (function bt:join-thread) threads)
    counter))


(testsuite locks/with-lock-held
  (loop
    :for (*default-lock-constructor*
          *default-condition-variable-constructor*)
      :in '((make-cas-lock make-cas-condition-variable)
            (make-bt-lock make-bt-condition-variable))
    :do

       (let ((nthreads 100)
             (niterations 1000))
         (testcase with-lock-held/lots-of-threads
                   :info (scat *default-lock-constructor* '/
                               *default-condition-variable-constructor* '/
                               "lots of threads incrementing a counter")
                   :expected (* nthreads niterations)
                   :actual (time (counter-increment-by-threads nthreads niterations))))

       (let ((nthreads 1)
             (niterations 100000))
         (testcase with-lock-held/single-thread
                   :info (scat *default-lock-constructor* '/
                               *default-condition-variable-constructor* '/
                               "single thread incrementing a counter")
                   :expected (* nthreads niterations)
                   :actual (time (counter-increment-by-threads nthreads niterations))))))


(testsuite lock/exclusion
  #|TODO|# "test that mutex is ensured by with-lock-held"
  )

(testsuite lock/locking-thread
  #|TODO|# "test that the locking-thread is recorded in the lock"
  )

(testsuite condition-variable/basic
  #|TODO|# "test basic condition-notify condition-wait behavior"
  )

(testsuite condition-variable/notification
  #|TODO|# "test  condition-notify condition-wait with multiple threads"
  )

