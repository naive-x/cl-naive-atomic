(in-package :cl-naive-atomic.tests)

(testsuite mvar/mono-thread

  (testcase *default-mvar-constructor*
            :expected nil
            :actual (not (boundp '*default-mvar-constructor*)))

  (testcase *default-lock-constructor*
            :expected nil
            :actual (not (boundp '*default-lock-constructor*)))

  (testcase *default-condition-variable-constructor*
            :expected nil
            :actual (not (boundp '*default-condition-variable-constructor*)))

  ;; Mono-thread: we should avoid blocking operations!

  (dolist (*default-mvar-constructor* *mvar-constructors*)

    (let ((mvar (make-mvar)))

      (testcase mvar-put/1
                :equal 'eql
                :expected 33
                :actual (complete-before 1 (put mvar 33))
                :info (scat *default-mvar-constructor*
                            " The result of (put mvar) is the new value"))

      (testcase mvar-take/1
                :equal 'eql
                :expected 33
                :actual (complete-before 1 (take mvar))
                :info (scat *default-mvar-constructor*
                            " The mvar take the put value."))

      (testcase mvar-setf-value/1
                :equal 'eql
                :expected 44
                :actual (complete-before 1 (setf (value mvar) 44))
                :info (scat *default-mvar-constructor*
                            " (setf (value mvar) new-value)"))

      (testcase mvar-value/1
                :equal 'eql
                :expected 44
                :actual (complete-before 1 (value mvar))
                :info (scat *default-mvar-constructor*
                            " The (value mvar) is take."))

      (testcase mvar-put-take/1
                :equal 'eql
                :expected 22
                :actual (complete-before 1 (progn (put mvar 22) (take mvar)))
                :info (scat *default-mvar-constructor*
                            " put take sequence.")))

    (let ((mvar (make-mvar 33)))

      (testcase mvar-take/2
                :equal 'eql
                :expected 33
                :actual (complete-before 1 (take mvar))
                :info (scat *default-mvar-constructor*
                            " The mvar take the put value."))

      (testcase mvar-setf-value/2
                :equal 'eql
                :expected 44
                :actual (complete-before 1 (setf (value mvar) 44))
                :info (scat *default-mvar-constructor*
                            " (setf (value mvar) new-value)"))

      (testcase mvar-value/2
                :equal 'eql
                :expected 44
                :actual (complete-before 1 (value mvar))
                :info (scat *default-mvar-constructor*
                            " The (value mvar) is take."))

      (testcase mvar-put-take/2
                :equal 'eql
                :expected 22
                :actual (complete-before 1 (progn (put mvar 22) (take mvar)))
                :info (scat *default-mvar-constructor*
                            " put take sequence.")))

    (let ((mvar (make-mvar 33)))
      (testcase mvar-put/full
                :equal 'eql
                :expected :timeout
                :actual (complete-before 2 (put mvar 22))
                :info (scat *default-mvar-constructor*
                            " put non-empty shall block.")))

    (let ((mvar (make-mvar)))
      (testcase mvar-take/empty
                :equal 'eql
                :expected :timeout
                :actual (complete-before 2 (take mvar))
                :info (scat *default-mvar-constructor*
                            " take empty shall block.")))

    ))

(testsuite mvar/multiple-thread
  #|TODO|# "test mvar with multiple threads")
