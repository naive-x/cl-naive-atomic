(in-package :cl-naive-atomic.tests)

(defparameter *mvar-constructors* '(make-mvar/cas make-mvar/lock))

(defun iota (count &optional (start 0) (step 1))
  "
RETURN:   A list containing the elements
          (start start+step ... start+(count-1)*step)
          The start and step parameters default to 0 and 1, respectively.
          This procedure takes its name from the APL primitive.
EXAMPLE:  (iota 5) => (0 1 2 3 4)
          (iota 5 0 -0.1) => (0 -0.1 -0.2 -0.3 -0.4)
"
  (loop
    :repeat count
    :for item = start :then (+ item step)
    :collect item))

(defmacro complete-before (seconds expression)
  (let ((vblock (gensym "COMPLETE-BEFORE-"))
        (vin-the-debugger (gensym "IN-THE-DEBUGGER-")))
    `(block ,vblock
       (let ((,vin-the-debugger nil))
         (declare (special ,vin-the-debugger))
         (handler-bind
             ((bt:timeout
                (lambda (err)
                  (declare (ignore err))
                  (unless ,vin-the-debugger
                    (return-from ,vblock :timeout)))))
           (bt:with-timeout (,seconds)
             (handler-bind
                 ((error
                    (lambda (err)
                      (when *debug*
                        (let ((,vin-the-debugger t))
                          (declare (special ,vin-the-debugger))
                          (invoke-debugger err))))))
               ,expression)))))))

(defun scat (&rest string-designators)
  (apply (function concatenate) 'string
         (mapcar (function string) string-designators)))

