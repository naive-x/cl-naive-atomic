(in-package :cl-naive-atomic.api)

(defconstant +empty+ '+empty+)

(defvar *default-mvar-constructor*)

(defun make-mvar (&optional (value +empty+) (constructor *default-mvar-constructor*))
  (funcall constructor :value value))

(defgeneric take         (var)
  (:documentation
   "Removes whatever is stored in the value slot and replaces it with +empty+.
If already empty TAKE blocks waiting for a value."))

(defgeneric put          (var new-value)
  (:documentation
   "If the value slot is +EMPTY+ puts NEW-VALUE into the slot.
If not +EMPTY+, blocks waiting for the slot to become empty.
Return the NEW-VALUE."))

(defgeneric value        (var))
(defgeneric (setf value) (new-value var))



(defvar *default-lock-constructor*)

(defun make-lock (&optional (name "Anonymous lock") (constructor *default-lock-constructor*))
  (funcall constructor :name name))

(defgeneric locking-thread (lock))
(defgeneric call-with-lock-held (lock thunk))

(defmacro with-lock-held ((lock) &body body)
  `(call-with-lock-held ,lock (lambda () ,@body)))


(defvar *default-condition-variable-constructor*)

(defun make-condition-variable (&key (name "Anonymous condition variable")
                                  (constructor *default-condition-variable-constructor*))
  (funcall constructor :name name))

(defgeneric condition-notify (condition-variable))
(defgeneric condition-wait (lock condition-variable &key timeout))

;;;; THE END ;;;;
