(in-package :cl-naive-mvar)

;; We cannot use DEFCLASS because COMPARE-AND-SWAP cannot work on CLOS slot places.
;; We must use DEFSTRUCT.

(defstruct (mvar
            (:predicate mvarp)
            (:constructor nil))
  (value +empty+))

(defstruct (mvar/cas
            (:include mvar))
  (lock nil))

(defstruct (mvar/lock
            (:include mvar))
  (lock           (make-lock "mvar")
   :read-only t)
  (read-possible  (make-condition-variable :name "mvar-read-possible")
   :read-only t)                   ; notified when reads are possible.
  (write-possible (make-condition-variable :name "mvar-write-possible")
   :read-only t))                  ; notified when writes are possible.


(defparameter *default-mvar-constructor* 'make-mvar/cas
  "The user may bind this parameter to a different MVAR constructor function.")


(defmethod value                  ((mvar mvar))   (take mvar))
(defmethod (setf value) (new-value (mvar mvar))   (put mvar new-value))


;;; ----------------------------------------
;;; mvar/cas methods:

(defmethod take ((mvar mvar/cas))
  (loop
     (let ((value +empty+))
       (cas-with-retry (mvar/cas-lock mvar) nil t)
       (rotatef value (slot-value mvar 'value))
       (compare-and-swap (mvar/cas-lock mvar) t nil)
       (if (eql value +empty+)
           (bt:thread-yield)
           (return value)))))

(defmethod put ((mvar mvar/cas) new-value)
  (assert (not (eql new-value +empty+)))
  (loop
     (cas-with-retry (mvar/cas-lock mvar) nil t)
     (if (eql (mvar-value mvar) +empty+)
         (progn
           (setf (mvar-value mvar) new-value)
           (compare-and-swap (mvar/cas-lock mvar) t nil)
           (return-from put new-value))
         (progn
           (compare-and-swap (mvar/cas-lock mvar) t nil)
           (bt:thread-yield)))))

(defmethod atomic-update ((mvar mvar/cas) update-func)
  "Update the MVAR value atomically, using the UPDATE-FUNC"
  (cas-with-retry (mvar/cas-lock mvar) nil t)
  (handler-bind
      ((error (lambda (err)
                (setf (mvar-value mvar) +empty+)
                (compare-and-swap (mvar/cas-lock mvar) t nil)
                (signal err))))
    (let ((new-value (funcall update-func (mvar-value mvar))))
      (setf (mvar-value mvar) new-value)
      (compare-and-swap (mvar/cas-lock mvar) t nil)
      new-value)))

;;; ----------------------------------------
;;; mvar/lock methods:

(defmethod take ((mvar mvar/lock))
  "Removes whatever is stored in the value slot and replaces it with +empty+.
If already empty take blocks waiting for a value."
  (let ((lock (mvar/lock-lock mvar)))
    (with-lock-held (lock)
      (loop :for value := (mvar-value mvar)
            :do (if (eql value +empty+)
                    (condition-wait lock (mvar/lock-read-possible mvar))
                    (progn
                      (setf (mvar-value mvar) +empty+)
                      (condition-notify (mvar/lock-write-possible mvar))
                      (return value)))))))

(defmethod put ((mvar mvar/lock) new-value)
  "If the value slot is +EMPTY+ puts NEW-VALUE into the slot.
If not +EMPTY+, blocks waiting for the slot to become empty."
  (assert (not (eql new-value +empty+)))
  (let ((lock (mvar/lock-lock mvar)))
    (with-lock-held (lock)
      (loop :for value := (mvar-value mvar)
            :do  (if (eq value +empty+)
                     (progn
                       (setf (mvar-value mvar) new-value)
                       (condition-notify (mvar/lock-read-possible mvar))
                       (return-from put new-value))
                     (condition-wait lock (mvar/lock-write-possible mvar) ))))))

(defmethod atomic-update ((mvar mvar/lock) update-func)
  "Update the MVAR value atomically, using the UPDATE-FUNC"
  (let ((lock (mvar/lock-lock mvar)))
    (with-lock-held (lock)
      (handler-bind
          ((error (lambda (err)
                    (setf (mvar-value mvar) +empty+)
                    (condition-notify (mvar/lock-write-possible mvar))
                    (signal err))))
        (let ((new-value (funcall update-func (mvar-value mvar))))
          (setf (mvar-value mvar) new-value)
          (if (eql +empty+ new-value)
              (condition-notify (mvar/lock-write-possible mvar))
              (condition-notify (mvar/lock-read-possible mvar)))
          new-value)))))

;;;; THE END ;;;;
