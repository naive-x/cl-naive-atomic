# -*- mode: org-mode;coding:utf-8 -*-

* cl-naive-atomic

Uses mvar from haskell fame and bordeaux-threads (locks and
conditional-variables) to create an atomic type/container.

Locks are expensive! So think long and hard on why you would want to
use atomic container. atomic container is fine grained locking and you
should use more coarse locking in general practice.

There is also an implementation independant (well at least for SBCL
and CCL) CAS that can be used instead of the heavier atom
container. (This should be in its own project ideally but I can't see
it ever being used by anybody else so should be fine in here for now.)

** Status

This package(s) has not taken a beating yet it needs more testing in
the field so consider it alpha-ish as of 14 Apr 2021.

** Some of the Why's

It uses thread locks, thats why the "naive" tag. Atomic operations are
usually implemented with compare-and-swap (CAS) and/or something like
without-interrupts and not thread locks because they are expensive.

The issue with the likes of without-interrupts is symetric
multiprocessing Franz has this to say about there
[[https://franz.com/support/documentation/current/doc/operators/excl/without-interrupts.htm][without-interrupts]].

I checked with the guys in #sbcl and the people present agreed that the
same issue exists for SBCL. The short and the sweet is that  "it
provides no cross-process protection of shared objects". If the data
you are sharing is small like a global counter you might never end up
experiecing any problems. If you are talking about a lot of data and
updates take longish you will get into trouble.

The same goes for CAS basically, the swap is safe but some thread
(from another process) might still be abusing the data so you could
end up with data in an inconsistent state.

** More About mvar's

I understand that you could implement mvar with CAS (compare and swap)
but for more complex data with longish updates you would still need to
use a lock.

To learn more about
[[https://learning.oreilly.com/library/view/parallel-and-concurrent/9781449335939/ch07.html][mvar's]]

My implementation of mvars was taken from [[http://pvk.ca/Blog/Lisp/concurrency_with_mvars.html][this blog]] by
Paul Khuong, links to the code are dead unfortunately. So no license
to copy, I did dig through any likely github projects from the author
but could not find the file.

** What else is out there?

When I decided to put this in its own library I hunted low and high
for a library written by some one that actually understands this
stuff. Unfortunately I could only find code in other projects se
below.

[[https://github.com/binghe/portable-threads/blob/master/portable-threads.lisp][Portable Threads]]

[[https://github.com/danlentz/cl-ctrie/blob/master/common-atomic.lisp][cl-ctrie]]

[[https://github.com/zkat/memento-mori/blob/develop/src/utils.lisp][memento-mori]]

None of the code was copied from these but it gave me some ideas.

There was [[http://kvardek-du.kerno.org/2012/06/augmenting-bordeaux-threads-with-atomic.html][talk]] (years ago) about atomic operations to enhance BT but
dont know what happened to that.

** TODO:

- write proper tests and pack them in their own file etc.
- sort out asdf and file names
- maybe hook up getx?

